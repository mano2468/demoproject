angular.module('starter.controllers', [])
    // APP
    .controller('AppCtrl', function($scope, $ionicModal, $rootScope) {
        var orderId = "1418394476";
        var merchantId = "dillip";
        var onEndUrlReached = function() {
            // your code to check the server status
            var paymentStatus = getPaymentStatusFromServer();
            if (paymentStatus == "CHARGED") {
                gotoThankYouPage();
            } else {
                gotoFailurePage();
            }
        };

        var gotoThankYouPage = function() {
           console.log('ThankYouPage');
        };

        var gotoFailurePage = function() {
            console.log('FailurePage');
        };

        var abortedCallback = function() {
            gotoFailurePage();
        };
        var onTransactionAborted = function() {
            console.log('TransactionAborted');
        };
        var endUrls = ["https://shop.com/juspay-response/.*", "https://beta.shop.com/juspay-response/.*"];
        $scope.Submit = function() {
            ExpressCheckout.startCheckoutActivity({
                "endUrls": endUrls,
                "onEndUrlReached": onEndUrlReached,
                "onTransactionAborted": abortedCallback,
                "environment": "PRODUCTION", // can be either of "SANDBOX" or "PRODUCTION"
                "parameters": {
                    "orderId": orderId,
                    "merchantId": merchantId,
                },
            });
        };

    });
